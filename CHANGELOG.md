
## 0.4.8 [11-11-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-aws_nfw!21

---

## 0.4.7 [10-15-2024]

* Changes made at 2024.10.14_20:00PM

See merge request itentialopensource/adapters/adapter-aws_nfw!20

---

## 0.4.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-aws_nfw!18

---

## 0.4.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-aws_nfw!17

---

## 0.4.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aws_nfw!16

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:08PM

See merge request itentialopensource/adapters/adapter-aws_nfw!15

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:21PM

See merge request itentialopensource/adapters/adapter-aws_nfw!14

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_15:33PM

See merge request itentialopensource/adapters/adapter-aws_nfw!13

---

## 0.4.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-aws_nfw!12

---

## 0.3.8 [03-27-2024]

* Changes made at 2024.03.27_13:39PM

See merge request itentialopensource/adapters/security/adapter-aws_nfw!11

---

## 0.3.7 [03-21-2024]

* Changes made at 2024.03.21_14:49PM

See merge request itentialopensource/adapters/security/adapter-aws_nfw!10

---

## 0.3.6 [03-13-2024]

* Changes made at 2024.03.13_11:44AM

See merge request itentialopensource/adapters/security/adapter-aws_nfw!9

---

## 0.3.5 [03-11-2024]

* Changes made at 2024.03.11_16:13PM

See merge request itentialopensource/adapters/security/adapter-aws_nfw!8

---

## 0.3.4 [03-06-2024]

* Add broker mappings

See merge request itentialopensource/adapters/security/adapter-aws_nfw!7

---

## 0.3.3 [02-27-2024]

* Changes made at 2024.02.27_11:46AM

See merge request itentialopensource/adapters/security/adapter-aws_nfw!6

---

## 0.3.2 [01-27-2024]

* added dynamic region support

See merge request itentialopensource/adapters/security/adapter-aws_nfw!5

---

## 0.3.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/security/adapter-aws_nfw!4

---

## 0.3.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-aws_nfw!3

---

## 0.2.0 [11-08-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-aws_nfw!3

---

## 0.1.3 [04-10-2023]

* Add content type header for auth

See merge request itentialopensource/adapters/security/adapter-aws_nfw!2

---

## 0.1.2 [07-13-2022]

* Categorization and System Name

See merge request itentialopensource/adapters/security/adapter-aws_nfw!1

---

## 0.1.1 [06-24-2022]

* Bug fixes and performance improvements

See commit dc2c4b3

---
