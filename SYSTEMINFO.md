# AWS Network Firewall

Vendor: AWS
Homepage: https://aws.amazon.com/

Product: AWS Network Firewall
Product Page: https://aws.amazon.com/network-firewall/

## Introduction
We classify AWS Network Firewall into the Security/SASE domain as AWS Network Firewall provides network security features.

"Network Firewall monitors and protects your Amazon VPC traffic."

## Why Integrate
The AWS Network Firewall adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Network Firewall to enhance network security and protect resources. 

With this adapter you have the ability to perform operations with AWS Network Firewall such as:

- Create Firewall
- Describe Firewall
- List Firewalls
- Create Rule Group
- Describe Rule Groups
- List Rule Groups
- Tag Resource

## Additional Product Documentation
The [API documents for AWS Network Firewall](https://docs.aws.amazon.com/network-firewall/latest/APIReference/Welcome.html)
