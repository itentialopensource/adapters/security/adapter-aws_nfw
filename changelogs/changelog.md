
## 0.1.3 [04-10-2023]

* Add content type header for auth

See merge request itentialopensource/adapters/security/adapter-aws_nfw!2

---

## 0.1.2 [07-13-2022]

* Categorization and System Name

See merge request itentialopensource/adapters/security/adapter-aws_nfw!1

---

## 0.1.1 [06-24-2022]

* Bug fixes and performance improvements

See commit dc2c4b3

---
