# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Aws_nfw System. The API that was used to build the adapter for Aws_nfw is usually available in the report directory of this adapter. The adapter utilizes the Aws_nfw API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS Network Firewall adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Network Firewall to enhance network security and protect resources. 

With this adapter you have the ability to perform operations with AWS Network Firewall such as:

- Create Firewall
- Describe Firewall
- List Firewalls
- Create Rule Group
- Describe Rule Groups
- List Rule Groups
- Tag Resource

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
